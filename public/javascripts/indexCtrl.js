'use strict';
var oroLite = angular.module("oroLite",[]);
oroLite.controller('indexCtrl', indexCtrl);

indexCtrl.$inject = ['$rootScope', '$scope', '$location', '$http', '$window'];

function indexCtrl($rootScope, $scope, $location,  $http, $window) {
	
	$scope.rel = "reload"

	$scope.login_block = false;
	$scope.signup_block = false;
	$scope.user_block = false;
	$scope.loggedIn = false;

	$scope.show_my_list_div = false;
	$scope.add_new_buddy_list_div = false;
	$scope.my_buddies_div = false;

	$scope.task_list_div = false;
	$scope.my_task_list_div = false;
	$scope.buddyList = [];
	$scope.tasks = [];
	$scope.myList = [];
	$scope.myListNew = [];
	$scope.pastBookingsList = [];
	$scope.pastIncompleteBookingList = [];
	$scope.recoHotels = [];
	$scope.current_list = {};
	$scope.current_hotel = "";
	$scope.booking_tab = false;
	$scope.booking_tab1 = true;
	$scope.current_booking_id = "";
	$scope.current_price = "";

	$scope.show_past_visits = false;
	$scope.past_bookings = false;
	$scope.past_incomplete_bookings = false;

	$scope.showPastVisits = function() {
		$scope.show_past_visits = true;
		$scope.past_bookings = false;
		$scope.past_incomplete_bookings = false;
	}
	$scope.pastBookings = function() {
		$scope.show_past_visits = false;
		$scope.past_bookings = true;
		$scope.past_incomplete_bookings = false;
	}
	$scope.pastIncompleteBooking = function() {
		$scope.show_past_visits = false;
		$scope.past_bookings = false;
		$scope.past_incomplete_bookings = true;
	}

	$scope.showBookingTab = function() {
		if ($scope.booking_tab) {
			$scope.booking_tab = false;
			$scope.booking_tab1 = true;

		} else {
			$scope.booking_tab1 = false;
			$scope.booking_tab = true;
			$scope.bookHotelStart();
		}
	}
	$scope.showMyListDiv = function() {
		$scope.show_my_list_div = true;
		$scope.add_new_nuddy_list_div = false;
		$scope.task_list_div = false;
	$scope.my_task_list_div = false;
		$scope.my_buddies_div = false;
	}

	$scope.showAddNewBuddyDiv = function() {
		$scope.show_my_list_div = false;
		$scope.add_new_nuddy_list_div = true;
		$scope.my_buddies_div = false;
	$scope.my_task_list_div = false;
		$scope.task_list_div = false;

	}

	$scope.showMyBuddiesDiv = function() {
		$scope.show_my_list_div = false;
		$scope.add_new_nuddy_list_div = false;
		$scope.my_buddies_div = true;
	$scope.my_task_list_div = false;
		$scope.task_list_div = false;

	}
	$scope.showTaskDiv = function() {
		$scope.show_my_list_div = false;
		$scope.add_new_nuddy_list_div = false;
		$scope.my_buddies_div = false;
		$scope.my_task_list_div = false;
		$scope.task_list_div = true;
	}
	$scope.showMyTaskDiv = function() {
		$scope.show_my_list_div = false;
		$scope.add_new_nuddy_list_div = false;
		$scope.my_buddies_div = false;
		$scope.my_task_list_div = true;
		$scope.task_list_div = false;
	}

	$scope.showLogin = function() {
		$scope.login_block = true;
		$scope.signup_block = false;
		$scope.user_block = false;
	}
	///// show-hide functions.
	$scope.showAdmin = function() {
		$scope.login_block = false;
		$scope.signup_block = true;
		$scope.user_block = false;
	}

	$scope.showUser = function() {
		$scope.login_block = false;
		$scope.signup_block = false;
		$scope.user_block = true;
	}

	$scope.initial_shows = function() {
		if (sessionStorage.getItem("login") !== null) {
			if (sessionStorage.getItem("login") === "yes") {
				$scope.loggedIn = true;
				if (sessionStorage.getItem("user_type") !== null && sessionStorage.getItem("user_type") === "admin") {
					$scope.showAdmin();
				}
				if (sessionStorage.getItem("user_type") !== null && sessionStorage.getItem("user_type") === "user") {

					$scope.showUser();
				}
			} else {
				$scope.showLogin();
			}
		} else {
			$scope.showLogin();
		}
	};
	$scope.initial_shows();

	$scope.logout = function() {
		sessionStorage.setItem("login", "no");
		sessionStorage.setItem("user_type", "none");
		$window.location.reload();


	}

	$scope.getInitialCheck = function(email,initial_password) {
		var data = {};
		if (!email || email.length === 0) {
			alert("Enter email to continue");
			return;
		}
		if (!initial_password || initial_password.length === 0) {
			alert("Enter password to proceed");
			return;
		}
		data.password = initial_password;
		data.email = email;
		$http.post('/v1/initial_checks/login', data).then(function (resp) {
			if (resp.data.status.response === "failure") {
				alert(resp.data.status.userMessage);
				return;
			}
				$scope.loggedIn = true;
				sessionStorage.setItem("login", "yes");
				sessionStorage.setItem("login_email", email);
				sessionStorage.setItem("user_type", "user");
				$scope.showUser();
		});
	};

	$scope.addNewUser = function(email,initial_password) {
		var data = {};
		if (!email || email.length === 0) {
			alert("Enter email to continue");
			return;
		}
		if (!initial_password || initial_password.length === 0) {
			alert("Enter password to proceed");
			return;
		}
		data.password = initial_password;
		data.email = email;
		$http.post('/v1/initial_checks/addNewUser', data).then(function (resp) {
			
		alert(resp.data.status.userMessage);
		$window.location.reload();

							
		});
	};

	$scope.showHotelList = function() {
		$scope.booking_tab = false;
		$scope.booking_tab1 = true;
		var email = sessionStorage.getItem("login_email");
		var data = {};
		data.email = email;
		$http.post('/v1/initial_checks/fetchHotelsList', data).then(function (resp) {
			$scope.myList = [];
			$scope.myList  = resp.data.data;
			$scope.showMyListDiv();				
		});
	}


	$scope.showMyTasks = function(index) {
		$scope.tasks = [];
		$scope.tasks = index.tasks;
		$scope.current_list = index;
		var data = {};
	    data.email = sessionStorage.getItem("login_email");
	    data.name = index.name;
		$http.post('/v1/initial_checks/hotelVisit', data).then(function (resp) {
			if (resp.data.status.response === "failure") {
				alert(resp.data.status.userMessage);
				$window.location.reload();
			} else {
				$scope.current_hotel = $scope.current_list.name;
				$scope.showMyTaskDiv();
			}
			
		});
	}
	$scope.showTasks = function(index) {
		$scope.tasks = [];
		$scope.tasks = index.tasks;
		$scope.current_list = index;
		$scope.showTaskDiv();
	}

	$scope.markDone = function(taskName) {
		var data = {};

	    data.createdFor = sessionStorage.getItem("login_email");
	    data.name = $scope.current_list.name;
	    data.taskName = taskName;
	    $http.post('/v1/initial_checks/markItDone', data).then(function (resp) {
			alert(resp.data.status.userMessage);
			$window.location.reload();
			
		});
	}

	$scope.bookHotelStart = function() {
		var data = {};
		data.hotelName = $scope.current_list.name;
		data.price = $scope.current_list.price;
		data.email = sessionStorage.getItem("login_email");
		$http.post('/v1/initial_checks/bookHotelStart', data).then(function (resp) {
			$scope.current_booking_id = resp.data.data;
		});
	};

	$scope.fetchPastVisits = function() {
		var data = {};
		data.email = sessionStorage.getItem("login_email");
		$http.post('/v1/initial_checks/fetchPastVisits', data).then(function (resp) {
			$scope.myListNew = [];
			$scope.myListNew  = resp.data.data;
			$scope.showPastVisits();
		});
	}

	$scope.fetchPastBooking = function() {
		var data = {};
		data.email = sessionStorage.getItem("login_email");
		$http.post('/v1/initial_checks/fetPastBookings', data).then(function (resp) {
			$scope.pastBookingsList = [];
			$scope.pastBookingsList  = resp.data.data;
			$scope.pastBookings();
		});
	}
	$scope.fetchIncomplete = function() {
		var data = {};
		data.email = sessionStorage.getItem("login_email");
		$http.post('/v1/initial_checks/fetchImcompleteBookings', data).then(function (resp) {
			$scope.pastIncompleteBookingList = [];
			$scope.pastIncompleteBookingList  = resp.data.data;
			$scope.pastIncompleteBooking();
		});
	}

	$scope.continueBooking = function(eachElement) {
		$scope.current_list = {};
		eachElement.name = eachElement.hotelName;
		$scope.current_list = eachElement;
		$scope.current_booking_id = eachElement.random_id;
		$scope.current_hotel = eachElement.name;
		$scope.showMyTaskDiv();

	}

	$scope.fetchReco = function() {
		var data = {};
		data.email = sessionStorage.getItem("login_email");
		$http.post('/v1/initial_checks/fetchRecommendations', data).then(function (resp) {
			$scope.recoHotels = [];
			$scope.recoHotels  = resp.data.data;
			console.log(resp.data);
			$scope.showMyBuddiesDiv();
		});
	}
	$scope.bookHotelNow = function(start_date, end_date,number_rooms) {
		if (start_date === undefined) {
			alert("Enter Start date first to continue");
			return;
		}
		if (end_date === undefined) {
			alert("Enter End date first to continue");
			return;
		}
		if (number_rooms === undefined) {
			alert("Enter number of rooms first to continue");
			return;
		}
		var oneDay = 24*60*60*1000;
		var diffDays = Math.round(Math.abs((end_date.getTime() - start_date.getTime())/(oneDay)));
		var cost = diffDays * $scope.current_list.price * number_rooms;
		$scope.current_price = cost;
		var data = {};
		data.price = $scope.current_list.price;
		data.start_date = getFormattedDate(start_date);
		data.end_date = getFormattedDate(end_date);
		data.duration = diffDays;
		data.cost = cost;
		data.hotelName = $scope.current_list.name;
		data.email = sessionStorage.getItem("login_email");
		data.custom_id = $scope.current_booking_id;
		$http.post('/v1/initial_checks/bookHotelNow', data).then(function (resp) {
			alert(resp.data.status.userMessage);
			$window.location.reload();
			
		});
		
	}

	var getFormattedDate = function(date) {
        var d = date.getDate();
        if (d <= 9) {
            d = "0" + d;
        }
        var m = date.getMonth() + 1;
        if (m <= 9) {
            m = "0" + m;
        }
        var y = date.getFullYear();
        return d + "-" + m + "-" + y;
    }
}
