var express = require('express');
var router = express.Router();
var randtoken = require('rand-token');
var multer = require('multer');
var rest = require('restler');
var randtoken = require('rand-token');
var MetaInspector = require('node-metainspector');
var extractor = require('article-extractor');
var db = require('../../../db/db');
var helper = require('../helper');

router.all('*', function(req, res, next) {
	console.log("starting query");
	next();
});

router.post('/addNewUser', function(req, res, next) {
    var email = helper.mysql_real_escape_string(req.body.email);
    var password = helper.mysql_real_escape_string(req.body.password);
    var collection = db.getCollection("users");
    collection.update({
            "email" : email
        },{
            $set: {
                    "email" :  email,
                    "password" : password
            }
        },{
            upsert: true
        }, function (err) {
            if (err) {
                return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
            } else {
            return res.send({
                "status" : {
                    "response" : "success",
                    "message" : "",
                    "userMessage" : "New user successfully added. Please login to continue"
                },"data" : {
                }
            });
            }
        }); 
})
router.post('/login', function(req, res, next) {
	var email = req.body.email;
    var password = req.body.password;
    
    var collection = db.getCollection("users");
    collection.find({
        "email" : email,
        "password" : password
    },{
        "email" : 1, "password" : 1
    }).toArray(function(err, docs) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        if (docs.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "",
                    "userMessage" : "Email password combination does not exist. Please try again"
                },"data" : {
                }
            });
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : ""
            },"data" : {
            }
        });
    });
});

router.post('/fetchHotelsList', function(req, res, next) {
    var email = req.body.email;
    var collection = db.getCollection("hotel_list");
    collection.find({
    },{

    }).toArray(function(err, docs) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : ""
            },"data" : docs
        });
    });
});

router.post('/bookHotelStart', function(req, res, next) {
    var hotelName = req.body.hotelName;
    var email = req.body.email;
    var price = req.body.price;
    var uni = randtoken.generate(40);
    var collection = db.getCollection("bookings");
    collection.update({
        "random_id" : uni
    },{
        "$set" : {
            "random_id" : uni,
            "hotelName" : hotelName,
            "email" : email,
            "price" : price
        }
    },{
        upsert : true
    }, function (err) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : "successfully booking started"
            },"data" : uni
        });
    });
});

router.post('/bookHotelNow', function(req, res, next) {
    var custom_id = req.body.custom_id;
    var start_date = req.body.start_date;
    var end_date = req.body.end_date;
    var duration = req.body.duration;
    var cost = req.body.cost;
    var hotelName = req.body.hotelName;
    var email = req.body.email;
    var price = req.body.price;

    var collection = db.getCollection("bookings");
    collection.update({
        "random_id" : custom_id
    },{
        "$set" : {
            "start_date" : start_date,
            "end_date" : end_date,
            "duration" : duration,
            "cost" : cost,
            "random_id" : custom_id,
            "hotelName" : hotelName,
            "email" : email,
            "price" : price,
            "completed" : true
        }
    },{
        upsert : true
    }, function (err) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : "Your  booking is done successfully."
            },"data" : {

            }
        });
    })
});

router.post('/fetchPastVisits', function(req, res, next) {
    var email = req.body.email;
    var collection = db.getCollection("users");
    var hotelCollection = db.getCollection("hotel_list");
    collection.find({
        "email" : email
    },{
        "email" : 1, "visited" : 1
    }).toArray(function(err, docs) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            });
        }
        if (docs.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "no visits",
                    "userMessage" : "no visits"
                },"data" : {
                }
            });
        }
        var visited = docs[0].visited;
        hotelCollection.find({
            "name" :  {$in: visited}
        }).toArray(function(err1, docs) {
            if (err) {
                return res.send({
                    "status" : {
                        "response" : "failure",
                        "message" : "err : " + err1,
                        "userMessage" : "There is some error. Please try again later"
                    },"data" : {
                    }
                }); 
            }
            return res.send({
                "status" : {
                    "response" : "success",
                    "message" : "",
                    "userMessage" : ""
                },"data" : docs
            });
        });
    });
});

router.post('/fetPastBookings', function(req, res, next) {
    var email = req.body.email;
    var collection = db.getCollection("bookings");
    collection.find({
        "email" : email
    },{

    }).toArray(function(err, docs) {
        if (err || docs.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : ",
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        var docs1 = [];
        for (var i = 0; i < docs.length; i++) {
            if (docs[i].completed) {
                docs1.push(docs[i]);
            }
        }
        if (docs1.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : ",
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : ""
            },"data" : docs1
        });
    });
});

router.post('/fetchImcompleteBookings', function(req, res, next) {
    var email = req.body.email;
    var collection = db.getCollection("bookings");
    collection.find({
        "email" : email
    },{

    }).toArray(function(err, docs) {
        if (err || docs.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : ",
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        var docs1 = [];
        for (var i = 0; i < docs.length; i++) {
            if (!docs[i].completed) {
                docs1.push(docs[i]);
            }
        }
        if (docs1.length === 0) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : ",
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : ""
            },"data" : docs1
        });
    });
});

router.post('/hotelVisit', function(req, res, next) {
    var email = req.body.email;
    var name = req.body.name;

    var collection = db.getCollection("users");
    collection.update({
        "email" : email
    },
    {
        $push: {
            "visited": name
        }
    },
    {
        upsert : true
    }, function (err) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : " + err,
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        return res.send({
            "status" : {
                "response" : "success",
                "message" : "",
                "userMessage" : "successfully visited hotel"
            },"data" : ""
        });
    });
});

var sortByKey = function(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

router.post('/fetchRecommendations', function(req, res, next) {
    var email = req.body.email;
    var collection = db.getCollection("bookings");
    var hotelCollection = db.getCollection("hotel_list");
    var num_reco = 5;
    collection.find({
        "email" : email
    },{

    }).toArray(function(err, docs) {
        if (err) {
            return res.send({
                "status" : {
                    "response" : "failure",
                    "message" : "err : ",
                    "userMessage" : "There is some error. Please try again later"
                },"data" : {
                }
            }); 
        }
        var docs1 = [];
        for (var i = 0; i < docs.length; i++) {
            if (docs[i].completed) {
                docs1.push(docs[i]);
            }
        }
        hotelCollection.find({
        },{

        }).toArray(function(err2, docs2) {
            if (err2 || docs2.length === 0) {
                return res.send({
                    "status" : {
                        "response" : "failure",
                        "message" : "err : ",
                        "userMessage" : "There is some error. Please try again later"
                    },"data" : {
                    }
                }); 
            }
            var av_price = 0
            for (var i = 0; i < docs2.length; i++) {
                av_price = av_price + docs2[i].price;
                if (docs2[i].price < 1000) {
                    docs2[i].type = "Economic";
                } else if (docs2[i].price >= 1000 && docs2[i].price <10000) {
                    docs2[i].type = "Business";
                } else {
                    docs2[i].type = "Platinum";
                }
            }

            av_price = av_price / docs2.length;
            if (docs1.length === 0) {
                for (var i = 0; i < docs2.length; i++) {
                    docs2[i].meanDiff = Math.abs(av_price - docs2[i].price);
                }
                docs2 = sortByKey(docs2,'meanDiff');
            } else {
                var user_av_price = 0;
                for (var i = 0; i < docs1.length; i++) {
                    user_av_price = user_av_price + docs1[i].price;
                }
                user_av_price = user_av_price / docs1.length;
                for (var i = 0; i < docs2.length; i++) {
                    docs2[i].meanDiff = Math.abs(user_av_price - docs2[i].price);
                }
                docs2 = sortByKey(docs2,'meanDiff');
            }
            var finalDocs = [];
            for (var i = 0; i < num_reco; i++) {
                finalDocs.push(docs2[i]);
            }
            return res.send({
                "status" : {
                    "response" : "success",
                    "message" : "",
                    "userMessage" : ""
                },"data" : finalDocs
            });
        });
    });
});
module.exports = router;