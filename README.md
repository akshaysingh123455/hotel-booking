# README #


Softwares/ stack requirements
1. Nodejs - development version v4.2.6
2. npm  - development version 3.5.2
3. mongo - development version 3.2.9


### How do I get set up? ###

-> clone repo / extract code
-> go to core code direction
-> run command "npm install"
-> run command "npm install bower -g"
-> run command "cd public/"
-> run command "bower install"
-> run command "cd .."
-> npm start

### Testing URL
http://localhost:5000/


#### Workflow of app
First page :Login page / Signup Page

After successfully Login /signup user dashboard will open
There are 3 Tabs
	1) Hotels : This  tab will include list of hotels in our database. User can select
	Hotel and do the booking after clicking on each hotel.

	2) My Profile : This tab contains user activity. it includes
		1) Past visited Hotels
		2) Past bookings
		3) Past Incomplete bookings (User can continue with them)

	3) Recommendations : This tab includes recommendations of hotels on the basis of user 
	activity. 

	Note: For now recommendations are based on only completed booking. Can be modified in v2


	Dummy Users : password
	aaryan : aaryan   (with some data)
	test123 : test123	(blank)

	DATABASE : I'm attaching exported mongodb database in directory name 'hotels'

	To importDatabase follow this command
	-> mongorestore -d hotels <directory_backup>


	UI has been kept as simple as possible. Use of internet is being done for testing session variables.